package aplicatiePractica;

import java.util.Scanner;

public class rezolvareProblema {
	public static void main(String[] args) {
		System.out.println("Program started.");
		Scanner inputUtilizator = new Scanner(System.in);
		try {
			long numarIntrodus = inputUtilizator.nextLong();
			System.out.println(getCifraMaxima(numarIntrodus));
		} catch(Exception e){
			System.out.println(e.getMessage());
		} finally {
			inputUtilizator.close();
		}
	}
	
	private static long getCifraMaxima(Long numarIntrodus){
		Long cifraMax = 0l;
		String[] cifreNumar = numarIntrodus.toString().split("");
		for(int i=0 ; i < cifreNumar.length ; i++) {
				cifraMax = cifraMax > Long.valueOf(cifreNumar[i]) ? cifraMax : Long.valueOf(cifreNumar[i]);
		}
		return cifraMax;
	}
}
